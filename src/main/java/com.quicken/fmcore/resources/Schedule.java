package com.quicken.fmcore.resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Schedule {
    private String startDate;
    private String frequency;
    private Integer interval;
    private Integer instances;
    private String endDate;
}
