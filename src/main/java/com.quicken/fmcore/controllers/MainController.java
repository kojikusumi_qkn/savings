package com.quicken.fmcore.controllers;

import com.quicken.fmcore.entities.Plan;
import com.quicken.fmcore.entities.recieved.PlanRequest;
import com.quicken.fmcore.entities.recieved.PublicTokenRequest;
import com.quicken.fmcore.entities.recieved.TransferRequest;
import com.quicken.fmcore.repositories.PlanRepository;
import com.quicken.fmcore.services.PlanService;
import org.hibernate.validator.constraints.pl.REGON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping
public class MainController {

    @Autowired
    private PlanService service;

    @GetMapping("/all")
    public @ResponseBody Iterable<Plan> findAll(){
        return service.gimmePlan();
    }

    @PostMapping("/accounts")
    public @ResponseBody
    Object getAccounts(@RequestBody PublicTokenRequest publicTokenRequest) {
        return service.getAccounts(publicTokenRequest.getPublicToken());
    }

    @PostMapping("/plan")
    public @ResponseBody
    Object makePlan(@RequestBody PlanRequest planRequest) {
        return service.makePlan(planRequest);
    }

    @PostMapping("/transfer")
    public @ResponseBody
    Object firstTransfer(@RequestBody TransferRequest transferRequest) {
        return service.transfer(transferRequest.getId(), true);
    }

    @PostMapping("/webhook")
    public @ResponseBody
    Object secondTransfer(@RequestBody TransferRequest transferRequest) {
        return service.transfer(transferRequest.getId(), false);
    }


    @GetMapping("/test")
    public @ResponseBody
    Object test() {
        return service.test();
    }

    @RequestMapping(
            value = "/**",
            method = RequestMethod.OPTIONS
    )
    public ResponseEntity handle() {
        System.out.println("Hello Peter");
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        return new ResponseEntity(headers, HttpStatus.OK);
    }
}
