package com.quicken.fmcore.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@ToString
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Account {
    @Id
    @Size(max=250)
    private String accountId;
    private String itemId;
    @Column(unique=true)
    @Size(max=250)
    private String fundingSource;
}
