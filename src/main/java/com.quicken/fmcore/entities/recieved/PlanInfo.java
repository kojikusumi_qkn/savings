package com.quicken.fmcore.entities.recieved;

import lombok.*;

import java.time.LocalDate;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PlanInfo {
    private String title;


    //schedule
    private LocalDate startDate;
    private String frequency;
    //was interval
    private Integer per;
    private Integer instances;
    private LocalDate endDate;

    //scheduleTimes
    private Integer byMonthDay;
    private String byDay;
    private Integer byMonth;
    private Integer byYearDay;

    //same as amountDue
    private Integer transAmt;
    private String fromAccountId;
    private String fromAccountName;
    private String toAccountName;
    private String toAccountId;

    private String fromItemId;
    private String toItemId;

    private LocalDate dueNextOn;
    private Integer instancesRemaining;

    private Integer saveGoal;
    private Integer totalSaved;

    private Integer paused;
    private String quickenId;
    private String lastTransDate;
    private String transDay;




}