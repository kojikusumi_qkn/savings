package com.quicken.fmcore.entities.recieved;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

@ToString
@AllArgsConstructor
@Getter
@Service
@NoArgsConstructor
public class DwollaAccessResponse {
    private String access_token;
    private String token_type;
    private Integer expires_in;
}
