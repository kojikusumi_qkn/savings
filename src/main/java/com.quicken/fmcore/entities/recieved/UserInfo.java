package com.quicken.fmcore.entities.recieved;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

@Getter
@Service
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {
    private String firstName;
    private String lastName;
    private String email;
    private String type;
    private String address1;
    private String city;
    private String state;
    private String postalCode;
    private String dateOfBirth;
    private String ssn;
    private String quickenId;
}
