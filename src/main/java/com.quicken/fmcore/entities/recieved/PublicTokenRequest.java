package com.quicken.fmcore.entities.recieved;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

@ToString
@NoArgsConstructor
@Getter
@Service
public class PublicTokenRequest {
    private String publicToken;
}
