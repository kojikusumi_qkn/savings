package com.quicken.fmcore.entities.recieved;


import lombok.*;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProcessorTokenResponse {
    private String processor_token;
    private String request_id;
}
