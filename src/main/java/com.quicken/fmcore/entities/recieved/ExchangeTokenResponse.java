package com.quicken.fmcore.entities.recieved;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

@ToString
@Getter
@Service
@NoArgsConstructor
public class ExchangeTokenResponse {
    private String access_token;
    private String item_id;
    private String request_id;
}
