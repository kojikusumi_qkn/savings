package com.quicken.fmcore.entities.recieved;

import com.quicken.fmcore.entities.Plan;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

@Getter
@Service
@NoArgsConstructor
@ToString
public class PlanRequest {
    private PlanInfo planInfo;
    private UserInfo userInfo;
}
