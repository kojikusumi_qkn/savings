package com.quicken.fmcore.entities.recieved;

import lombok.*;

@ToString
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class TransferRequest {
    private Long id;

}
