package com.quicken.fmcore.entities;

import com.quicken.fmcore.entities.recieved.PlanInfo;
import com.quicken.fmcore.resources.Schedule;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

import java.time.LocalDate;

@ToString
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Plan {
    @Id
    @GeneratedValue
    private Long id;
    private String title;


    //schedule
    private LocalDate startDate;
    private String frequency;
    //was interval
    private Integer per;
    private Integer instances;
    private LocalDate endDate;

    /*
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private Schedule schedule;
    */

    //scheduleTimes
    private Integer byMonthDay;
    private String byDay;
    private Integer byMonth;
    private Integer byYearDay;

    //same as amountDue
    private Integer transAmt;
    private String fromAccountId;
    private String fromAccountName;
    private String toAccountName;
    private String toAccountId;

    private LocalDate dueNextOn;
    private Integer instancesRemaining;

    private Integer saveGoal;
    private Integer totalSaved;
    private Integer paused;
    private String quickenId;
    private String fromItemId;
    private String toItemId;
    private String lastTransDate;
    private String transDay;

    public Plan(PlanInfo planInfo)
    {
        this.title = planInfo.getTitle();
        this.startDate = planInfo.getStartDate();
        this.frequency = planInfo.getFrequency();
        this.per = planInfo.getPer();
        this.instances = planInfo.getInstances();
        this.endDate = planInfo.getEndDate();
        this.byMonthDay = planInfo.getByMonthDay();
        this.byDay = planInfo.getByDay();
        this.byMonth = planInfo.getByMonth();
        this.byYearDay = planInfo.getByYearDay();
        this.transAmt = planInfo.getTransAmt();
        this.fromAccountId = planInfo.getFromAccountId();
        this.toAccountId = planInfo.getToAccountId();
        this.dueNextOn = planInfo.getDueNextOn();
        this.instancesRemaining = planInfo.getInstancesRemaining();
        this.saveGoal = planInfo.getSaveGoal();
        this.paused = 0;
        this.quickenId = planInfo.getQuickenId();
        this.totalSaved = planInfo.getTotalSaved();
        this.fromItemId = planInfo.getFromItemId();
        this.toItemId = planInfo.getToItemId();
        this.lastTransDate = planInfo.getLastTransDate();
        this.fromAccountName = planInfo.getFromAccountName();
        this.toAccountName = planInfo.getToAccountName();
        this.transDay = planInfo.getTransDay();
    }



}
