package com.quicken.fmcore.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@ToString
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DwollaCustomer {
    @Id
    @Size(max=250)
    private String customerUri;
    private String balanceUri;
}
