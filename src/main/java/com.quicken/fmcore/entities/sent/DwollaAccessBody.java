package com.quicken.fmcore.entities.sent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Service
public class DwollaAccessBody {
    private String client_id;
    private String client_secret;
    private String grant_type = "client_credentials";

    public DwollaAccessBody(String client_id, String client_secret) {
        this.client_id = client_id;
        this.client_secret = client_secret;
    }
}
