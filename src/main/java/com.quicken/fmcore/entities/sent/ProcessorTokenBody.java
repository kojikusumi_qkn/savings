package com.quicken.fmcore.entities.sent;

import lombok.*;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProcessorTokenBody {
    private String client_id;
    private String secret;
    private String access_token;
    private String account_id;
}
