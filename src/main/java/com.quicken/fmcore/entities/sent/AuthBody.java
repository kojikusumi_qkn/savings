package com.quicken.fmcore.entities.sent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Service
public class AuthBody {
    private String client_id;
    private String secret;
    private String access_token;
}
