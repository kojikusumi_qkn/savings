package com.quicken.fmcore.entities.sent;

import lombok.*;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FundingSourceBody {
    private String plaidToken;
    private String name;
}
