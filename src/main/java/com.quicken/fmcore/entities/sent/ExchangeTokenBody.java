package com.quicken.fmcore.entities.sent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

@ToString
@Getter
@Service
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeTokenBody {
    private String client_id;
    private String secret;
    private String public_token;
}
