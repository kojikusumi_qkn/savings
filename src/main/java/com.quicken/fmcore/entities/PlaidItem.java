package com.quicken.fmcore.entities;

import lombok.*;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@ToString
@Entity
@Getter
@Setter
@NoArgsConstructor
public class PlaidItem {

    @Id
    @Size(max=250)
    private String access_token;
    @Column(unique=true)
    @Size(max=250)
    private String item_id;
}
