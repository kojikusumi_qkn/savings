package com.quicken.fmcore.repositories;

import com.quicken.fmcore.entities.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, String> {
}
