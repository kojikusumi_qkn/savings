package com.quicken.fmcore.repositories;

import com.quicken.fmcore.entities.PlaidItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface ItemRepository extends CrudRepository<PlaidItem, String> {
    @Query("SELECT access_token FROM PlaidItem WHERE item_id=?1")
    String findAccessByItem(String item_id);
}
