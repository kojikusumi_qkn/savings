package com.quicken.fmcore.repositories;

import com.quicken.fmcore.entities.Plan;
import org.springframework.data.repository.CrudRepository;

public interface PlanRepository extends CrudRepository<Plan, Long> {
}
