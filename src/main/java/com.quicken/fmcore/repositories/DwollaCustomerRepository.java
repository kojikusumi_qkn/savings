package com.quicken.fmcore.repositories;

import com.quicken.fmcore.entities.DwollaCustomer;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface DwollaCustomerRepository extends CrudRepository<DwollaCustomer, String> {
    Optional<DwollaCustomer> findFirstByOrderByCustomerUri();
}
