package com.quicken.fmcore.services;

import com.quicken.fmcore.entities.Account;
import com.quicken.fmcore.entities.DwollaCustomer;
import com.quicken.fmcore.entities.PlaidItem;
import com.quicken.fmcore.entities.Plan;
import com.quicken.fmcore.entities.recieved.*;
import com.quicken.fmcore.entities.sent.*;
import com.quicken.fmcore.repositories.AccountRepository;
import com.quicken.fmcore.repositories.DwollaCustomerRepository;
import com.quicken.fmcore.repositories.ItemRepository;
import com.quicken.fmcore.repositories.PlanRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.sql.SQLOutput;
import java.util.Optional;
import java.util.UUID;

//TODO:naming of dwolla accounts makes no sense right now


@Service
public class PlanService {


    @Autowired
    private PlanRepository repository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private DwollaCustomerRepository dwollaCustomerRepository;
    @Autowired
    private AccountRepository accountRepository;

    private RestTemplate restTemplate = new RestTemplate();
    private static final String baseUrl = "https://sandbox.plaid.com";
    private static final String client_id ="5b3c07477a5a9f0014ed05f1";
    private static final String secret ="2e9d824234de4fd0ab79bc67e7b730";

    private static final String baseDwolla = "https://api-sandbox.dwolla.com";
    private static final String dwollaKey = "8NJiVzCsCW0MWq7NIdLSxLuTRYNUEdnJIEB6S91YrpvogIhAzm";
    private static final String dwollaSecret = "lldjPQxc0BFoXOywmx4laUjxaewM3jIbO8PTNA888GvOqKO3ET";


    public Iterable<Plan> findAll() {
        return repository.findAll();
    }

    public Object getAccounts(String publicToken) {
        String access_token = exchange(publicToken);
        return auth(access_token);
    }

    public Object makePlan(PlanRequest planRequest) {
        UserInfo userInfo = planRequest.getUserInfo();
        PlanInfo planInfo = planRequest.getPlanInfo();
        System.out.println("Before toStrings and such...XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        System.out.println(planRequest.toString());
        System.out.println(planInfo.toString());
        String customerUri = getCustomer(userInfo);

        makeFundingSources(planInfo, customerUri);

        /*
        String fromProccesorToken = makeProcessorToken(planInfo.getFromItemId(), planInfo.getFromAccountId());
        String toProccessorToken = makeProcessorToken(planInfo.getToItemId(), planInfo.getToAccountId());


        URI fromFundingSource = makeFundingSource(customerUri, fromProccesorToken, planInfo.getTitle(), false);
        URI toFundingSource = makeFundingSource(customerUri, toProccessorToken, planInfo.getTitle(), true);
        */

        Plan plan = new Plan(planInfo);
        repository.save(plan);


        return repository.findAll();
    }

    public Iterable<Plan> gimmePlan(){
        return repository.findAll();
    }
    public Object test() {
        return "test";
    }

    //TODO:this does nothing as of right now, should initiate a transfer
    public Object transfer(Long id, boolean first) {
        String resourceUrl = baseDwolla + "/transfers";
        Optional<Plan> plan = repository.findById(id);
        if(!plan.isPresent())
            return "Could not find plan";

        Optional<Account> account = accountRepository.findById(first ? plan.get().getFromAccountId() : plan.get().getToAccountId());
        if(!account.isPresent())
            return "Account(s) missing";

        Optional<DwollaCustomer> customer = dwollaCustomerRepository.findFirstByOrderByCustomerUri();
        if(!customer.isPresent())
            return "Missing customer";

        String sourceUri = first ? account.get().getFundingSource() : customer.get().getBalanceUri();
        String destinationUri = first ? customer.get().getBalanceUri() : account.get().getFundingSource();

        HttpHeaders headers = makeDwollaHeaders();
        JSONObject body = new JSONObject();
        try {
            body
                    .put("_links", new JSONObject()
                            .put("source", new JSONObject()
                                    .put("href", sourceUri))
                            .put("destination", new JSONObject()
                                    .put("href", destinationUri)))
                    .put("amount", new JSONObject()
                            .put("currency", "USD")
                            .put("value", plan.get().getTransAmt()));
            HttpEntity<String> entity = new HttpEntity<>(body.toString(),headers);
            return restTemplate.postForLocation(resourceUrl, entity, Object.class);
        }
        catch (JSONException e) {
            return "Error making json body for transfer: could not complete request";
        }
    }

    //exchanges a plaid public_token for an access_token
    private String exchange(String publicToken) {
        String resourceUrl = baseUrl + "/item/public_token/exchange";
        ExchangeTokenBody body = new ExchangeTokenBody(client_id, secret, publicToken);
        ResponseEntity<PlaidItem> response = restTemplate.postForEntity(resourceUrl, body, PlaidItem.class);
        itemRepository.save(response.getBody());

       return response.getBody().getAccess_token();
    }

    private Object auth(String accessToken) {
        String resourceUrl = baseUrl + "/auth/get";
        AuthBody body = new AuthBody(client_id, secret, accessToken);
        ResponseEntity<Object> authResponse = restTemplate.postForEntity(resourceUrl, body, Object.class);
        return authResponse.getBody();
    }

    private String getDwollaAccess(){
        String resourceUrl = "https://sandbox.dwolla.com/oauth/v2/token";
        DwollaAccessBody body = new DwollaAccessBody(dwollaKey, dwollaSecret);
        ResponseEntity<DwollaAccessResponse> response = restTemplate.postForEntity(resourceUrl, body, DwollaAccessResponse.class);
        return response.getBody().getAccess_token();
    }

    //TODO:currently only supports one customer, this is hilariously bad
    private String getCustomer(UserInfo userInfo) {
        Optional<DwollaCustomer> stored = dwollaCustomerRepository.findFirstByOrderByCustomerUri();
        if(stored.isPresent())
            return stored.get().getCustomerUri();
        else {
            String customerUri = makeCustomer(userInfo);
            String balanceUri = getBalanceUri(customerUri);
            dwollaCustomerRepository.save(new DwollaCustomer(customerUri, balanceUri));
            return customerUri;
        }
    }

    private String makeCustomer(UserInfo userInfo) {
        String resourceUrl = baseDwolla + "/customers";
        HttpHeaders headers = makeDwollaHeaders();
        HttpEntity<UserInfo> entity = new HttpEntity<>(userInfo, headers);
        System.out.println(userInfo.toString());
        System.out.println(entity.toString());
        URI location = restTemplate.postForLocation(resourceUrl, entity);
        return location.toString();

    }

    private String getBalanceUri(String customerUri) {
        String resourceUrl = customerUri + "/funding-sources";
        HttpHeaders headers = makeDwollaHeaders();
        HttpEntity<String> body = new HttpEntity<>("",headers);
        HttpEntity<String> response = restTemplate.exchange(resourceUrl, HttpMethod.GET, body, String.class);
        try {
            JSONObject object = new JSONObject(response.getBody());
            return object
                    .getJSONObject("_embedded")
                    .getJSONArray("funding-sources")
                    .getJSONObject(0)
                    .getJSONObject("_links")
                    .getJSONObject("self")
                    .getString("href");

        }
        catch (JSONException e) {
            return "JSONError";
        }

    }

    private String makeProcessorToken(String itemId, String accountId) {
        String resourceUrl = baseUrl + "/processor/dwolla/processor_token/create";
        String accessToken = itemRepository.findAccessByItem(itemId);
        ProcessorTokenBody body = new ProcessorTokenBody(client_id, secret, accessToken, accountId);
        System.out.println(body.toString());
        ResponseEntity<ProcessorTokenResponse> response = restTemplate.postForEntity(resourceUrl, body, ProcessorTokenResponse.class);
        return response.getBody().getProcessor_token();
    }

    /*
    private URI makeFundingSource(String customerUri, String plaidToken, String name, Boolean to) {
        HttpHeaders headers = makeDwollaHeaders();
        FundingSourceBody body = new FundingSourceBody(plaidToken, ((to ? "to" : "from") + name));
        HttpEntity<FundingSourceBody> entity = new HttpEntity<>(body, headers);
        URI location = restTemplate.postForLocation(customerUri + "/funding-sources" ,entity);
        return location;
    }
    */

    private void makeFundingSources(PlanInfo planInfo, String customerUri){
        makeFundingSource(planInfo, customerUri, true);
        makeFundingSource(planInfo, customerUri, false);
    }

    private void makeFundingSource(PlanInfo planInfo, String customerUri, Boolean from) {
        String accountId = from ? planInfo.getFromAccountId() : planInfo.getToAccountId();
        String itemId = from ? planInfo.getFromItemId() : planInfo.getToItemId();

        Optional<Account> account = accountRepository.findById(accountId);
        if(!account.isPresent()) {
            String proccesorToken = makeProcessorToken(itemId, accountId);
            HttpHeaders headers = makeDwollaHeaders();
            FundingSourceBody body = new FundingSourceBody(proccesorToken, ((from ? "from" : "to") + planInfo.getTitle()));
            HttpEntity<FundingSourceBody> entity = new HttpEntity<>(body, headers);
            System.out.println("Before making a funding sources +##########################");
            System.out.println(customerUri);
            System.out.println(entity.toString());
             String location = restTemplate.postForLocation(customerUri + "/funding-sources" , entity).toString();
            Account newAccount = new Account(accountId, itemId, location);
            accountRepository.save(newAccount);
        }
    }

    private HttpHeaders makeDwollaHeaders() {
        String dwolla_access_token = getDwollaAccess();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/vnd.dwolla.v1.hal+json");
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", "Bearer " + dwolla_access_token);
        headers.set("Idempotency-Key", String.valueOf(UUID.randomUUID()));
        return headers;
    }




}
