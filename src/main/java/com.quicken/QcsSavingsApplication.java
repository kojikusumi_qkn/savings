package com.quicken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QcsSavingsApplication {

    public static void main(String[] args) {
        SpringApplication.run(QcsSavingsApplication.class, args);
    }
}
